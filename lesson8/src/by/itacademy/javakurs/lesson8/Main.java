package by.itacademy.javakurs.lesson8;

import by.itacademy.javakurs.lesson8.menu.MenuDisplay;

import java.util.InputMismatchException;

public class Main {
    public static void main(String[] args) {
        MenuDisplay menu = new MenuDisplay();
        menu.execute();

    }
}
