package by.itacademy.javakurs.lesson8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeSet;

public class Text {
    public ArrayList<String> list = new ArrayList<>();
    public TreeSet<String> treeSet = new TreeSet<>(new SortedByLength());
    public HashMap<String,Integer> hashMap = new HashMap<>();
    public String str;

    public Text() {
        str = "Текст для примера";
        toList();
        sort();
        calculateRepeats();
    }

    public Text(String str) {
        this.str = str;
        toList();
        sort();
        calculateRepeats();
    }

    public void calculateRepeats() {
        for (String i: treeSet) {
            for (String j : list) {
                if (i.equals(j)) {
                    hashMap.putIfAbsent(i,0);
                    hashMap.put(i,hashMap.get(i)+1);
                }
            }
        }
    }

    public void toList() {
        String[] s = str.split(" ");
        list.addAll(Arrays.asList(s));
    }

    public void sort() {
        treeSet.addAll(list);
    }

}
