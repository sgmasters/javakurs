package by.itacademy.javakurs.lesson8.classtask;

import java.util.HashMap;
import java.util.TreeMap;

public class MainKey {
    public static void main(String[] args) {

        HashMap<Key,String> hashMap = new HashMap<>();
        TreeMap<Key,String> treeMap = new TreeMap<>();
        Key key1 = new Key("0","First");
        hashMap.put(key1, "вещь1");
        Key key2 = new Key("0","First");
        hashMap.put(key2, "вещь4");
        treeMap.put(key1, "вещь1");
        treeMap.put(key2, "вещь4");
        System.out.println("hash:");
        for (Key key: hashMap.keySet()) {
            System.out.println(hashMap.get(key));
        }
        System.out.println("tree:");
        for (Key key: treeMap.keySet()) {
            System.out.println(treeMap.get(key));
        }
    }
}
