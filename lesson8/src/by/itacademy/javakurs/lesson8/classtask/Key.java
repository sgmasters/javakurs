package by.itacademy.javakurs.lesson8.classtask;

import java.util.Objects;


public class Key implements Comparable {
    private String id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return Objects.equals(id, key.id) &&
                Objects.equals(name, key.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }


    @Override
    public int compareTo(Object o) {
        Key key = (Key) o;
        if (this.equals(o)) {
            return 0;
        } else {
            return (id.equals(key.id)) ? name.compareTo(key.name) : id.compareTo(key.id);
        }

    }

    public Key(String id, String name) {
        this.id = id;
        this.name = name;
    }

}
