package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.Text;

public class MenuExit implements MenuItem {

    @Override
    public String name() {
        return "Exit";
    }

    @Override
    public void execute(Text text) {
        return;
    }
}
