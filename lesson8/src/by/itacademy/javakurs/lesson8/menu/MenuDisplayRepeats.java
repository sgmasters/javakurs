package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.operations.DisplayRepeats;

public class MenuDisplayRepeats extends MenuCommonOperation implements MenuItem {

    private String name = "Display repeats by key word";

    public MenuDisplayRepeats(RootMenu rootMenuItem) {
        super(new DisplayRepeats(), rootMenuItem);
    }

    @Override
    public String name() {
        return name;
    }

}
