package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.Text;
import by.itacademy.javakurs.lesson8.operations.Operation;


public abstract class MenuCommonOperation implements MenuItem {

    private Operation operation;
    private RootMenu rootMenuItem;

    public MenuCommonOperation(Operation operations, RootMenu rootMenuItem) {
        this.operation = operations;
        this.rootMenuItem = rootMenuItem;
    }

    @Override
    public void execute(Text text) {
        operation.execute(text);
        System.out.println();
        rootMenuItem.execute();
    }

}



