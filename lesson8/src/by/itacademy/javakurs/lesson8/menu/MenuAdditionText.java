package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.operations.AdditionText;

public class MenuAdditionText extends MenuCommonOperation implements MenuItem {

    private String name = "AdditionText";

    public MenuAdditionText(RootMenu rootMenuItem) {
        super(new AdditionText(),rootMenuItem);
    }

    @Override
    public String name() {
        return name;
    }

}
