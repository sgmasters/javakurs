package by.itacademy.javakurs.lesson8.menu;

public interface RootMenu {

    void execute();
}
