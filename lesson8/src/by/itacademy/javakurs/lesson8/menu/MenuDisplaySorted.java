package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.operations.DisplaySorted;

public class MenuDisplaySorted extends MenuCommonOperation implements MenuItem {

    public MenuDisplaySorted(RootMenu rootMenuItem) {
        super(new DisplaySorted(), rootMenuItem);
    }

    @Override
    public String name() {
        return "Display sorted words";
    }

}
