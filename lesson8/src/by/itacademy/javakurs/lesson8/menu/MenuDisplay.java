package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.Text;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuDisplay implements RootMenu {

    private static final Scanner SCANNER = new Scanner(System.in);

    private MenuItem[] subMenus = {
            new MenuAdditionText(this),
            new MenuDisplayText(this),
            new MenuDisplaySorted(this),
            new MenuDisplayRepeats(this),
            new MenuExit()
    };

    public static Text text = new Text();

    @Override
    public void execute() {
        for (int i = 0; i < subMenus.length; i++) {
            System.out.println((i + 1) + " " + subMenus[i].name());
        }
        try {
            subMenus[Integer.valueOf(SCANNER.next()) - 1].execute(text);
        } catch (NumberFormatException | InputMismatchException | IndexOutOfBoundsException e) {
            System.out.println("Выберите корректный элемент меню");
            this.execute();
        }
    }
}
