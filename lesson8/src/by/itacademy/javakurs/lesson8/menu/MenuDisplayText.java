package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.operations.DisplayText;

public class MenuDisplayText extends MenuCommonOperation implements MenuItem {

    public MenuDisplayText(RootMenu rootMenuItem) {
        super(new DisplayText(), rootMenuItem);
    }

    @Override
    public String name() {
        return "DisplayText";
    }


}
