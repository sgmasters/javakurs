package by.itacademy.javakurs.lesson8.menu;

import by.itacademy.javakurs.lesson8.Text;

public interface MenuItem {
    String name();
    void execute(Text text);
}
