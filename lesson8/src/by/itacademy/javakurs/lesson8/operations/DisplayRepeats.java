package by.itacademy.javakurs.lesson8.operations;

import by.itacademy.javakurs.lesson8.Text;

import java.util.Scanner;

public class DisplayRepeats implements Operation{

    private Scanner scanner  = new Scanner(System.in);

    @Override
    public void execute(Text text) {
        System.out.println("Input key word>>");
        Integer result = text.hashMap.get(scanner.next());
        System.out.println((result!=null)?result:"Нет такого слова в списке");
    }
}
