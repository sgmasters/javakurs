package by.itacademy.javakurs.lesson8.operations;

import by.itacademy.javakurs.lesson8.Text;

public class DisplaySorted implements Operation {

    @Override
    public void execute(Text text) {
        for (String i : text.treeSet) {
            System.out.println(i);
        }
    }
}
