package by.itacademy.javakurs.lesson8.operations;

import by.itacademy.javakurs.lesson8.Text;
import by.itacademy.javakurs.lesson8.menu.MenuDisplay;

import java.util.Scanner;

public class AdditionText implements Operation {

    @Override
    public void execute(Text text) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите текст: ");
        MenuDisplay.text = new Text(sc.nextLine());
    }

}
