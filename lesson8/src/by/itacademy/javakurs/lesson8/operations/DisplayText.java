package by.itacademy.javakurs.lesson8.operations;

import by.itacademy.javakurs.lesson8.Text;

public class DisplayText implements Operation {

    @Override
    public void execute(Text text) {
        for (String s:text.list)
        System.out.println(s);
    }
}
