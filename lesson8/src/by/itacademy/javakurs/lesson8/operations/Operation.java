package by.itacademy.javakurs.lesson8.operations;

import by.itacademy.javakurs.lesson8.Text;

public interface Operation {
    void execute(Text text);
}
