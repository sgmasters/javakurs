package by.itacademy.javakurs.lesson8;

import java.util.TreeSet;

/**
 * Created by user on 02.10.2018.
 */
public class WordsOutput {
    private TreeSet<String> treeSet = new TreeSet<>(new SortedByLength());

    public void execute(String s) {
        String[] str = s.split(" ");
        for (String item : str) {
            treeSet.add(item);
        }
        for (String item : treeSet) {
            System.out.println(item);
        }
    }
}
