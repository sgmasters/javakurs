package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Patient;
import by.itacademy.javakurs.task.Registry;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveToFile implements Actions {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("patientsOut.xml"))) {
            XMLOutputFactory output = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = output.createXMLStreamWriter(dataOutputStream);
            writer.writeStartDocument("UTF-8", "1.0");
            writer.writeCharacters("\n");
            writer.writeStartElement("registry");
            writer.writeCharacters("\n");
            for (Patient p : registry.getPatients().keySet()) {
                writer.writeStartElement("patient");
                writer.writeCharacters("\n");
                writer.writeStartElement("name");
                writer.writeCharacters(p.getName());
                writer.writeEndElement();
                writer.writeCharacters("\n");
                writer.writeStartElement("soname");
                writer.writeCharacters(p.getSoname());
                writer.writeEndElement();
                writer.writeCharacters("\n");
                writer.writeStartElement("dateOfBirth");
                writer.writeCharacters(p.getDateOfBirth().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
                writer.writeEndElement();
                writer.writeCharacters("\n");
                writer.writeStartElement("isIll");
                writer.writeCharacters(String.valueOf(registry.getPatients().get(p)));
                writer.writeEndElement();
                writer.writeCharacters("\n");
                writer.writeEndElement();
                writer.writeCharacters("\n");
            }
            writer.writeEndElement();
            writer.writeCharacters("\n");
            writer.writeEndDocument();
            writer.flush();
        } catch (XMLStreamException | IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Save to file";
    }
}
