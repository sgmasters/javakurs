package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Registry;

public interface Actions {
    void execute(Registry registry);

    String getName();
}
