package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Registry;

public class Exit implements Actions {
    @Override
    public void execute(Registry registry) {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "Exit";
    }
}
