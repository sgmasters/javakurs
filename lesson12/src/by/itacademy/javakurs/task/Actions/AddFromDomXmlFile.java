package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Patient;
import by.itacademy.javakurs.task.Registry;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddFromDomXmlFile implements Actions {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse("patients.xml");
            document.getDocumentElement().normalize();
            NodeList patients = document.getElementsByTagName("patient");
            for (int i = 0; i < patients.getLength(); i++) {
                Element element = (Element) patients.item(i);
                registry.addPatient(new Patient(
                                element.getElementsByTagName("name").item(0).getTextContent(),
                                element.getElementsByTagName("soname").item(0).getTextContent(),
                                LocalDate.parse(element.getElementsByTagName("dateOfBirth").item(0).getTextContent(), DateTimeFormatter.ofPattern("dd.MM.yyyy"))),
                        Boolean.parseBoolean(element.getElementsByTagName("isIll").item(0).getTextContent()));
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Add patients from DOM xml file";
    }
}
