package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Patient;
import by.itacademy.javakurs.task.Registry;

import java.time.LocalDate;
import java.util.Scanner;

public class AddFromConsole implements Actions {
    private static final Scanner SCANNER = new Scanner(System.in);

    public void execute(Registry registry) {
        System.out.println("Введите данные о пациенте");
        addPatient(registry);
    }

    @Override
    public String getName() {
        return "Add patient from console";
    }

    private void addPatient(Registry registry) {
        System.out.println("name>>");
        String name = SCANNER.next();
        System.out.println("soname>>");
        String soName = SCANNER.next();
        System.out.println("date of birth in format: yyyy MM dd>>");
        LocalDate dateOfBirth = LocalDate.of(
                Integer.parseInt(SCANNER.next()),
                Integer.parseInt(SCANNER.next()),
                Integer.parseInt(SCANNER.next())
        );
        System.out.println("is ill? (true/false)>>");
        boolean isIll = SCANNER.nextBoolean();
        registry.addPatient(new Patient(name, soName, dateOfBirth), isIll);
    }


}
