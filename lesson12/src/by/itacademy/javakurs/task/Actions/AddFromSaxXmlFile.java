package by.itacademy.javakurs.task.Actions;

import by.itacademy.javakurs.task.Patient;
import by.itacademy.javakurs.task.Registry;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddFromSaxXmlFile implements Actions {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.getParser();
            DefaultHandler handler = new DefaultHandler() {
                boolean name = false;
                boolean soname = false;
                boolean dateOfBirth = false;
                boolean isIll = false;
                String patientName;
                String patientSoName;
                LocalDate patientDateOfBirth;
                boolean patientIsIll;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    if (qName.equals("name")) {
                        name = true;
                    } else if (qName.equals("soname")) {
                        soname = true;
                    } else if (qName.equals("dateOfBirth")) {
                        dateOfBirth = true;
                    } else if (qName.equals("isIll")) {
                        isIll = true;
                    }
                }

                @Override
                public void characters(char ch[], int start, int length) throws SAXException {
                    if (name) {
                        patientName = new String(ch, start, length);
                        name = false;
                    } else if (soname) {
                        patientSoName = new String(ch, start, length);
                        soname = false;
                    } else if (dateOfBirth) {
                        patientDateOfBirth = LocalDate.parse(new String(ch, start, length), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                        dateOfBirth = false;
                    } else if (isIll) {
                        patientIsIll = Boolean.parseBoolean(new String(ch, start, length));
                        registry.addPatient(new Patient(patientName, patientSoName, patientDateOfBirth), patientIsIll);
                        isIll = false;
                    }
                }
            };
            saxParser.parse("patients.xml", handler);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Add patients from SAX xml file";
    }
}
