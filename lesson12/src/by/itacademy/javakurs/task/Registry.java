package by.itacademy.javakurs.task;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private Map<Patient, Boolean> patients = new HashMap<>();

    public void addPatient(Patient patient, Boolean isIll) {
        patients.put(patient, isIll);
    }

    public Map<Patient, Boolean> getPatients() {
        return patients;
    }
}
