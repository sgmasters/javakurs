package by.itacademy.javakurs.lesson12;


import java.util.ArrayList;

public class WorkerList {
    private ArrayList<Worker> workers = new ArrayList<>();

    public ArrayList<Worker> getWorkers() {
        return workers;
    }

    @Override
    public String toString() {
        return "WorkerList{" +
                "workers=" + workers +
                '}';
    }

    public void add(Worker worker) {
        workers.add(worker);
    }
}
