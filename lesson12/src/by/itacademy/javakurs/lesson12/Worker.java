package by.itacademy.javakurs.lesson12;

public class Worker {
    private String fio;
    private int experience;
    private String position;

    @Override
    public String toString() {
        return "Worker{" +
                "fio='" + fio + '\'' +
                ", experience=" + experience +
                ", position='" + position + '\'' +
                '}';
    }

    public Worker(String fio, int experience, String position) {
        this.fio = fio;
        this.experience = experience;
        this.position = position;
    }
}
