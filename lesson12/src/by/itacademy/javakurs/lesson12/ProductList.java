package by.itacademy.javakurs.lesson12;

import java.util.ArrayList;

public class ProductList {
    private ArrayList<Product> products = new ArrayList<>();

    public ArrayList<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        return "ProductList{" +
                "products=" + products +
                '}';
    }

    public void add(Product product) {
        products.add(product);
    }
}
