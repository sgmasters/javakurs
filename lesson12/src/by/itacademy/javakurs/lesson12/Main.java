package by.itacademy.javakurs.lesson12;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class Main {
    public static void main(String[] args) {
        Shop shop = new Shop();
        try {
            File inputFile = new File("shop.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            NodeList products = doc.getElementsByTagName("product");
            for (int i = 0; i < products.getLength(); i++) {
                Node nNode = products.item(i);
                Element element = (Element) nNode;
                shop.getProducts().add(new Product(
                        element.getElementsByTagName("name").item(0).getTextContent(),
                        Integer.parseInt(element.getElementsByTagName("price").item(0).getTextContent()),
                        Integer.parseInt(element.getElementsByTagName("number").item(0).getTextContent())));
            }
            NodeList workers = doc.getElementsByTagName("worker");
            for (int i = 0; i < workers.getLength(); i++) {
                Node nNode = workers.item(i);
                Element element = (Element) nNode;
                shop.getWorkers().add(new Worker(
                        element.getElementsByTagName("fio").item(0).getTextContent(),
                        Integer.parseInt(element.getElementsByTagName("experience").item(0).getTextContent()),
                        element.getElementsByTagName("position").item(0).getTextContent()));
            }
            System.out.println(shop.getProducts());
            System.out.println(shop.getWorkers());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
