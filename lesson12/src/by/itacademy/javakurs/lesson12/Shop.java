package by.itacademy.javakurs.lesson12;

public class Shop {
    private ProductList products = new ProductList();

    private WorkerList workers = new WorkerList();

    public ProductList getProducts() {
        return products;
    }

    public WorkerList getWorkers() {
        return workers;
    }
}
