package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.Complexity;
import by.itacademy.kursproject.domain.MaterialCosts;
import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class SearchByPriceRangeTest {
    private ServiceList serviceList = new ServiceList().clear();

    @Test
    public void executeShouldEqualsString() {
        Service service = new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0);
        serviceList.addService(service);
        String result = new SearchByPriceRange(0, 4).execute(serviceList);
        String expectedResult = "\n" + service.toString() + "\n";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void executeShouldNotEqualsString() {
        Service service = new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0);
        Service anotherService = new Service("another name", 4, 53, Complexity.HARD, MaterialCosts.AVERAGE, 1);
        serviceList.addService(service).addService(anotherService);
        String result = new SearchByPriceRange(0, 52).execute(serviceList);
        String expectedResult = service.toString() + anotherService.toString();
        Assert.assertNotEquals(expectedResult, result);
    }
}
