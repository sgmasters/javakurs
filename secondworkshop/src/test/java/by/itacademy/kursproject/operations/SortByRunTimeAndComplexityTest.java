package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.Complexity;
import by.itacademy.kursproject.domain.MaterialCosts;
import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class SortByRunTimeAndComplexityTest {

    @Test
    public void executeShouldEqualsObject() {
        ServiceList serviceList = new ServiceList().clear();
        serviceList.addService(new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0))
                .addService(new Service("another name", 422, 53, Complexity.SIMPLE, MaterialCosts.AVERAGE, 1))
                .addService(new Service("third name", 234, 234254, Complexity.MEDIUM, MaterialCosts.LOW, 2));
        ServiceList sorted = serviceList.sort(new SortByRunTimeAndComplexity().getComparator());
        ServiceList expected = new ServiceList().clear().addService(new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0))
                .addService(new Service("third name", 234, 234254, Complexity.MEDIUM, MaterialCosts.LOW, 2))
                .addService(new Service("another name", 422, 53, Complexity.SIMPLE, MaterialCosts.AVERAGE, 1));
        Assert.assertEquals(expected, sorted);
    }

    @Test
    public void executeShouldNotEqualsObject() {
        ServiceList serviceList = new ServiceList().clear();
        serviceList.addService(new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0))
                .addService(new Service("another name", 422, 53, Complexity.SIMPLE, MaterialCosts.AVERAGE, 1))
                .addService(new Service("third name", 234, 234254, Complexity.MEDIUM, MaterialCosts.LOW, 2));
        ServiceList sorted = serviceList.sort(new SortByRunTimeAndComplexity().getComparator());
        ServiceList expected = new ServiceList().clear().addService(new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0))
                .addService(new Service("another name", 422, 53, Complexity.SIMPLE, MaterialCosts.AVERAGE, 1))
                .addService(new Service("third name", 234, 234254, Complexity.MEDIUM, MaterialCosts.LOW, 2));
        Assert.assertNotEquals(expected, sorted);
    }
}
