package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.Complexity;
import by.itacademy.kursproject.domain.MaterialCosts;
import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class SearchByNameTest {
    private ServiceList serviceList = new ServiceList().clear();

    @Test
    public void executeShouldEqualsString() {
        Service service = new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0);
        serviceList.addService(service);
        String result = new SearchByName("some name").execute(serviceList);
        String expectedResult = service.toString();
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void executeShouldNotEqualsString() {
        Service service = new Service("some name", 2, 3, Complexity.HARD, MaterialCosts.AVERAGE, 0);
        Service anotherService = new Service("some name", 4, 53, Complexity.HARD, MaterialCosts.AVERAGE, 1);
        serviceList.addService(service).addService(anotherService);
        String result = new SearchByName("some name").execute(serviceList);
        String expectedResult = service.toString();
        Assert.assertNotEquals(expectedResult, result);
    }
}
