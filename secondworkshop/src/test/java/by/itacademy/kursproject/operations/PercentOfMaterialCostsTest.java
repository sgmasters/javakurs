package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class PercentOfMaterialCostsTest {
    private ServiceList serviceList = new ServiceList();

    @Test
    public void executeShouldEqualsString() {
        String result = new PercentOfMaterialCosts().execute(serviceList);
        String expectedResult = "NOTHING:\n" +
                "33\n" +
                "LOW:\n" +
                "16\n" +
                "AVERAGE:\n" +
                "33\n" +
                "HIGH:\n" +
                "16\n";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void executeShouldNotEqualsString() {
        String result = new PercentOfMaterialCosts().execute(serviceList);
        String expectedResult = "NOTHING:\n" +
                "34\n" +
                "LOW:\n" +
                "16\n" +
                "AVERAGE:\n" +
                "33\n" +
                "HIGH:\n" +
                "16\n";
        Assert.assertNotEquals(expectedResult, result);
    }
}
