package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class TopPriceForEachComplexityTest {
    private ServiceList serviceList = new ServiceList();

    @Test
    public void executeShouldBeEqualsString() {
        String result = new TopPriceForEachComplexity().execute(serviceList);
        String expectedResult =
                "Самые дорогие услуги по каждому из типов \"сложности\" SIMPLE:\n" +
                        "\n" +
                        "id=1, \n" +
                        "name='Creation 2D layout by template', \n" +
                        "runTime=0.5, \n" +
                        "price=20, \n" +
                        "complexity=SIMPLE, \n" +
                        "materialCosts=NOTHING\n" +
                        "\n" +
                        "Самые дорогие услуги по каждому из типов \"сложности\" MEDIUM:\n" +
                        "\n" +
                        "id=2, \n" +
                        "name='Workshop rent', \n" +
                        "runTime=8.0, \n" +
                        "price=1000, \n" +
                        "complexity=MEDIUM, \n" +
                        "materialCosts=AVERAGE\n" +
                        "\n" +
                        "Самые дорогие услуги по каждому из типов \"сложности\" HARD:\n" +
                        "\n" +
                        "id=6, \n" +
                        "name='Repair light box', \n" +
                        "runTime=15.0, \n" +
                        "price=1200, \n" +
                        "complexity=HARD, \n" +
                        "materialCosts=AVERAGE\n" +
                        "\n";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void executeShouldNotEqualsString() {
        String result = new TopPriceForEachComplexity().execute(serviceList);
        String expectedResult = "Самые дорогие услуги по каждому из типов \"сложности\" SIMPLE:\n" +
                "\n" +
                "Самые дорогие услуги по каждому из типов \"сложности\" MEDIUM:\n" +
                "\n" +
                "Самые дорогие услуги по каждому из типов \"сложности\" HARD:\n" +
                "\n";
        Assert.assertNotEquals(expectedResult, result);
    }
}
