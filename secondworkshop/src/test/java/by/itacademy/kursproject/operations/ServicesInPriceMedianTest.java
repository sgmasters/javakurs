package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.ServiceList;
import org.junit.Assert;
import org.junit.Test;

public class ServicesInPriceMedianTest {
    private ServiceList serviceList = new ServiceList();

    @Test
    public void executeShouldEqualsString() {
        String result = new ServicesInPriceMedian().execute(serviceList);
        String expectedResult = "медиана цен = 1000\n" +
                "Услуги, попадающие в медиану цен:\n" +
                "id=2, \n" +
                "name='Workshop rent', \n" +
                "runTime=8.0, \n" +
                "price=1000, \n" +
                "complexity=MEDIUM, \n" +
                "materialCosts=AVERAGE\n" +
                "\n" +
                "id=3, \n" +
                "name='Engraving', \n" +
                "runTime=1.0, \n" +
                "price=1000, \n" +
                "complexity=HARD, \n" +
                "materialCosts=LOW\n";
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void executeShouldNotEqualsString() {
        String result = new ServicesInPriceMedian().execute(serviceList);
        String expectedResult = "медиана цен = 500\n" +
                "Услуги, попадающие в медиану цен:\n" +
                "id=4, \n" +
                "name='Creation 3D layout', \n" +
                "runTime=12.0, \n" +
                "price=500, \n" +
                "complexity=HARD, \n" +
                "materialCosts=NOTHING\n";
        Assert.assertNotEquals(expectedResult, result);
    }
}
