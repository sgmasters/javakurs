package by.itacademy.kursproject.io.serialize;

import by.itacademy.kursproject.domain.Complexity;
import by.itacademy.kursproject.domain.MaterialCosts;
import by.itacademy.kursproject.domain.Service;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ServiceDeserializer implements JsonDeserializer<Service> {
    @Override
    public Service deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String name = jsonObject.get("name").getAsString();
        double runTime = jsonObject.get("runTime").getAsDouble();
        int price = jsonObject.get("price").getAsInt();
        Complexity complexity = Complexity.valueOf(jsonObject.get("complexity").getAsString());
        MaterialCosts materialCosts = MaterialCosts.valueOf(jsonObject.get("materialCost").getAsString());
        int id = jsonObject.get("id").getAsInt();
        return new Service(name, runTime, price, complexity, materialCosts, id);
    }
}
