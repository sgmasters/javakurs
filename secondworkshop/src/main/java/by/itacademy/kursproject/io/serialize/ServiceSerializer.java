package by.itacademy.kursproject.io.serialize;

import by.itacademy.kursproject.domain.Service;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class ServiceSerializer implements JsonSerializer<Service> {

    @Override
    public JsonElement serialize(Service service, Type type, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.addProperty("name", service.getName());
        json.addProperty("runTime", service.getRunTime());
        json.addProperty("price", service.getPrice());
        json.addProperty("complexity", service.getComplexity().name());
        json.addProperty("materialCost", service.getMaterialCosts().name());
        json.addProperty("id", service.getId());
        return json;
    }
}
