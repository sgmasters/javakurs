package by.itacademy.kursproject.io;

import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.io.serialize.ServiceDeserializer;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import java.io.FileReader;
import java.io.IOException;

public class ReadFromJson {
    private final String path;
    private ServiceList serviceList;

    public ReadFromJson(String path) {
        this.path = path;
    }

    public ServiceList execute() {
        try (JsonReader jsonReader = new JsonReader(new FileReader(path))) {
            serviceList = new GsonBuilder().
                    registerTypeAdapter(Service.class, new ServiceDeserializer())
                    .create()
                    .fromJson(jsonReader, ServiceList.class);
        } catch (IOException e) {
            e.getMessage();
        }
        return serviceList;
    }
}
