package by.itacademy.kursproject.io;

import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.io.serialize.ServiceSerializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import java.io.FileWriter;
import java.io.IOException;

public class SaveToJson extends Thread {

    private final String path;

    public SaveToJson(String path) {
        this.path = path;
    }

    public void execute(ServiceList serviceList) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Service.class, new ServiceSerializer())
                .create();
        try (JsonWriter jsw = new JsonWriter(new FileWriter(path))) {
            jsw.jsonValue(gson.toJson(serviceList)).flush();
        } catch (IOException e) {
            e.getMessage();
        }
    }

}
