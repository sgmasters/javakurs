package by.itacademy.kursproject.menu;

import by.itacademy.kursproject.domain.ServiceList;

public interface Menu {
    void execute(ServiceList serviceList);
}
