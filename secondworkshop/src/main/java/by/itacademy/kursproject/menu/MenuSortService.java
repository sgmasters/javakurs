package by.itacademy.kursproject.menu;

import by.itacademy.kursproject.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.menu.root.RootMenuItem;
import by.itacademy.kursproject.operations.SortById;
import by.itacademy.kursproject.operations.SortByRunTimeAndComplexity;

@MenuName("sortServices")
public class MenuSortService extends MenuCommonOperation implements Menu {
    private static final MenuOperation[] subMenus = {
            new SortById(),
            new SortByRunTimeAndComplexity()
    };

    public MenuSortService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

}
