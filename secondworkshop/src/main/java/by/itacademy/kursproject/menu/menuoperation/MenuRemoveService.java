package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.operations.RemoveService;

@MenuName("MenuRemoveService")
public class MenuRemoveService extends ManageService {

    private int id;

    public void execute(ServiceList serviceList) {
        id = serviceList.getServices().size() + 1;
        new RemoveService(getServiceData()).execute(serviceList);
    }

    @Override
    public int getId() {
        return id;
    }
}
