package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.operations.TopPriceForEachComplexity;

@MenuName("MenuTopPriceForEachComplexity")
public class MenuTopPriceForComplexity implements MenuOperation {

    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(new TopPriceForEachComplexity().execute(serviceList));
    }

}
