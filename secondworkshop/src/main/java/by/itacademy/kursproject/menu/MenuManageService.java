package by.itacademy.kursproject.menu;

import by.itacademy.kursproject.menu.menuoperation.MenuAdditionService;
import by.itacademy.kursproject.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.menu.menuoperation.MenuRemoveService;
import by.itacademy.kursproject.menu.root.RootMenuItem;

@MenuName("manageService")
public class MenuManageService extends MenuCommonOperation implements Menu {
    private static final MenuOperation[] subMenus = {
            new MenuAdditionService(),
            new MenuRemoveService()
    };

    public MenuManageService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

}
