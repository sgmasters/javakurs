package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;

import java.util.Comparator;

@MenuName("sortServices")
public abstract class SortService implements MenuOperation {

    public void execute(ServiceList serviceList) {
        serviceList.sort(getComparator());
        System.out.println(serviceList.getServices());
    }

    protected abstract Comparator getComparator();

}
