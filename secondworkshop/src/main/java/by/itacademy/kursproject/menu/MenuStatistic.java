package by.itacademy.kursproject.menu;

import by.itacademy.kursproject.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.menu.menuoperation.MenuPercentForEachMaterialCost;
import by.itacademy.kursproject.menu.menuoperation.MenuServicesInPriceMedian;
import by.itacademy.kursproject.menu.menuoperation.MenuTopPriceForComplexity;
import by.itacademy.kursproject.menu.root.RootMenuItem;

@MenuName("Statistic")
public class MenuStatistic extends MenuCommonOperation {
    private static final MenuOperation[] subMenus = {
            new MenuPercentForEachMaterialCost(),
            new MenuServicesInPriceMedian(),
            new MenuTopPriceForComplexity()
    };

    public MenuStatistic(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

}
