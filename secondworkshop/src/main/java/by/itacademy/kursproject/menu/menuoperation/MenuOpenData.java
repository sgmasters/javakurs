package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.io.ReadFromJson;
import by.itacademy.kursproject.menu.Menu;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.menu.root.RootMenuItem;

import java.util.Objects;

@MenuName("MenuOpenData")
public class MenuOpenData implements Menu {
    private RootMenuItem rootMenuItem;

    public MenuOpenData(RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
    }

    @Override
    public void execute(ServiceList serviceList) throws NullPointerException {
        serviceList.clear();
        serviceList.addAll(new ReadFromJson(Objects.requireNonNull(
                this.getClass().getClassLoader().getResource("data.json")).getFile())
                .execute().getServices());
        rootMenuItem.execute();
    }
}
