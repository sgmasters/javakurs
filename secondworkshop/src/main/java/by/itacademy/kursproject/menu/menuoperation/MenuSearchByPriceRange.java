package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.operations.SearchByPriceRange;

import java.util.ResourceBundle;
import java.util.Scanner;

@MenuName("SearchByPriceRange")
public class MenuSearchByPriceRange implements MenuOperation {
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(ResourceBundle.getBundle("data").getString("InputMinPrice") + ":");
        int min = Integer.parseInt(SCANNER.next());
        System.out.println(ResourceBundle.getBundle("data").getString("InputMaxPrice") + ":");
        int max = Integer.parseInt(SCANNER.next());
        System.out.println(new SearchByPriceRange(min, max).execute(serviceList));
    }

}
