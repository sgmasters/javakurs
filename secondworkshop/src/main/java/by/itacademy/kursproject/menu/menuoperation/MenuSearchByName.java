package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.operations.SearchByName;

import java.util.ResourceBundle;
import java.util.Scanner;

@MenuName("SearchByName")
public class MenuSearchByName implements MenuOperation {
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void execute(ServiceList serviceList) throws NullPointerException {
        System.out.println(ResourceBundle.getBundle("data").getString("inputName") + ":");
        System.out.println(new SearchByName(SCANNER.nextLine()).execute(serviceList));
    }

}
