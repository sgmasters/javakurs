package by.itacademy.kursproject.menu;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.io.SaveToJson;

import java.util.Objects;

@MenuName("exit")
public class MenuExit implements Menu {

    @Override
    public void execute(ServiceList serviceList) {
        new SaveToJson(Objects.requireNonNull(
                this.getClass().getClassLoader().getResource("data.json")).getFile())
                .execute(serviceList);
        System.exit(0);
    }

}
