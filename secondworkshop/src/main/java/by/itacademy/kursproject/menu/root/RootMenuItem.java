package by.itacademy.kursproject.menu.root;

import java.util.Locale;

public interface RootMenuItem {
    void execute();

    Locale getLocale();
}
