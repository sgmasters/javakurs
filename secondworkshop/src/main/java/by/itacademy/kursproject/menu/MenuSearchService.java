package by.itacademy.kursproject.menu;


import by.itacademy.kursproject.menu.menuoperation.MenuOperation;
import by.itacademy.kursproject.menu.menuoperation.MenuSearchByName;
import by.itacademy.kursproject.menu.menuoperation.MenuSearchByPriceRange;
import by.itacademy.kursproject.menu.root.RootMenuItem;

@MenuName("searchService")
public class MenuSearchService extends MenuCommonOperation implements Menu {
    private static final MenuOperation[] subMenus = {
            new MenuSearchByName(),
            new MenuSearchByPriceRange()
    };

    public MenuSearchService(RootMenuItem rootMenuItem) {
        super(subMenus, rootMenuItem);
    }

}
