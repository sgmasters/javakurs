package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.operations.ServicesInPriceMedian;

@MenuName("MenuServicesInPriceMedian")
public class MenuServicesInPriceMedian implements MenuOperation {
    @Override
    public void execute(ServiceList serviceList) {
        System.out.println(new ServicesInPriceMedian().execute(serviceList));
    }

}
