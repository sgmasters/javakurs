package by.itacademy.kursproject.menu.root;

import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.menu.Menu;
import by.itacademy.kursproject.menu.MenuExit;
import by.itacademy.kursproject.menu.MenuManageService;
import by.itacademy.kursproject.menu.MenuName;
import by.itacademy.kursproject.menu.MenuSearchService;
import by.itacademy.kursproject.menu.MenuSortService;
import by.itacademy.kursproject.menu.MenuStatistic;
import by.itacademy.kursproject.menu.menuoperation.MenuOpenData;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RootMenu implements RootMenuItem {

    private RootMenuItem rootMenuItem;
    private static final Scanner SCANNER = new Scanner(System.in);
    private Menu[] menus = {
            new MenuOpenData(this),
            new MenuManageService(this),
            new MenuSearchService(this),
            new MenuSortService(this),
            new MenuStatistic(this),
            new MenuExit()
    };
    private static final Logger LOGGER = Logger.getLogger(RootMenu.class.getName());
    private ServiceList serviceList = new ServiceList();

    public RootMenu(RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
    }

    public void execute() {
        rootMenuItem.getLocale();
        StringBuilder s = new StringBuilder(getBundle().getString("chooseOperation") + ":\n");
        for (int i = 0; i < menus.length; i++) {
            s.append(i + 1).append(" ").append(
                    ResourceBundle.getBundle("data").getString(
                            menus[i].getClass().getAnnotation(MenuName.class).value())
            ).append("\n");
        }
        System.out.print(s);
        try {
            menus[Integer.valueOf(SCANNER.next()) - 1].execute(serviceList);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            System.err.println("Incorrect Menu Element");
            LOGGER.log(Level.INFO, e.getMessage());
            this.execute();
        } catch (NullPointerException e1) {
            LOGGER.log(Level.INFO, e1.getMessage());
            this.execute();
        }
    }

    private ResourceBundle getBundle() {
        return ResourceBundle.getBundle("data");
    }

    @Override
    public Locale getLocale() {
        return rootMenuItem.getLocale();
    }
}
