package by.itacademy.kursproject.menu.menuoperation;

import by.itacademy.kursproject.domain.ServiceList;

public interface MenuOperation {
    void execute(ServiceList serviceList);
}
