package by.itacademy.kursproject;

import by.itacademy.kursproject.menu.root.LocaleChoiceMenuItem;
import by.itacademy.kursproject.menu.root.RootMenuItem;

public class App {
    public static void main(String[] args) {
        RootMenuItem rootMenuItem = new LocaleChoiceMenuItem();
        rootMenuItem.execute();
    }
}
