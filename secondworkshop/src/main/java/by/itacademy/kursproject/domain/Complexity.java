package by.itacademy.kursproject.domain;

public enum Complexity {
    SIMPLE,
    MEDIUM,
    HARD
}
