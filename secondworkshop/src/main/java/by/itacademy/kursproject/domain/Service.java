package by.itacademy.kursproject.domain;

import java.util.Objects;

public class Service {
    private String name;
    private double runTime;
    private int price;
    private Complexity complexity;
    private MaterialCosts materialCosts;
    private int id;

    public Service(String name, double runTime, int price, Complexity complexity, MaterialCosts materialCosts, int id) {
        this.name = name;
        this.runTime = runTime;
        this.price = price;
        this.complexity = complexity;
        this.materialCosts = materialCosts;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("\n");
        s.append("id=" + id +
                ", \nname='" + name + '\'' +
                ", \nrunTime=" + runTime +
                ", \nprice=" + price +
                ", \ncomplexity=" + complexity +
                ", \nmaterialCosts=" + materialCosts +
                "\n");
        return s.toString();
    }

    public int getPrice() {
        return price;
    }

    public Complexity getComplexity() {
        return complexity;
    }

    public MaterialCosts getMaterialCosts() {
        return materialCosts;
    }

    public double getRunTime() {
        return runTime;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return Double.compare(service.runTime, runTime) == 0 &&
                price == service.price &&
                id == service.id &&
                Objects.equals(name, service.name) &&
                complexity == service.complexity &&
                materialCosts == service.materialCosts;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, runTime, price, complexity, materialCosts, id);
    }
}
