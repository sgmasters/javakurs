package by.itacademy.kursproject.domain;

public enum MaterialCosts {
    NOTHING,
    LOW,
    AVERAGE,
    HIGH
}
