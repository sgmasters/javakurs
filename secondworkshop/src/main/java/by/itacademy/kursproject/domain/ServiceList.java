package by.itacademy.kursproject.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class ServiceList {

    private ArrayList<Service> services = new ArrayList<>(Arrays.asList(
            new Service("Creation 2D layout by template", 0.5, 20, Complexity.SIMPLE, MaterialCosts.NOTHING, 1),
            new Service("Workshop rent", 8, 1000, Complexity.MEDIUM, MaterialCosts.AVERAGE, 2),
            new Service("Engraving", 1, 1000, Complexity.HARD, MaterialCosts.LOW, 3),
            new Service("Creation 3D layout", 12, 500, Complexity.HARD, MaterialCosts.NOTHING, 4),
            new Service("Manufacturing metallic details", 6, 200, Complexity.HARD, MaterialCosts.HIGH, 5),
            new Service("Repair light box", 15, 1200, Complexity.HARD, MaterialCosts.AVERAGE, 6)
    ));

    public ServiceList addService(Service service) {
        this.services.add(service);
        return this;
    }

    public ServiceList sort(Comparator comparator) {
        services.sort(comparator);
        return this;
    }

    public ServiceList clear() {
        services.clear();
        return this;
    }

    public ServiceList addAll(ArrayList<Service> list) {
        services.addAll(list);
        return this;
    }

    public void removeService(Service service) {
        this.services.remove(service);
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ServiceList that = (ServiceList) o;
        return Objects.equals(services, that.services);
    }

    @Override
    public int hashCode() {
        return Objects.hash(services);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ServiceList{");
        sb.append("services=").append(services);
        sb.append('}');
        return sb.toString();
    }
}
