package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.comparators.ById;
import by.itacademy.kursproject.menu.menuoperation.SortService;

import java.util.Comparator;

public class SortById extends SortService {

    @Override
    protected Comparator getComparator() {
        return new ById();
    }

}
