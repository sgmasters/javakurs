package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class SearchByName implements Operation {
    private Map<String, Service> serviceMap = new HashMap<>();
    private String searchKey;

    public SearchByName(String searchKey) {
        this.searchKey = searchKey;
    }

    @Override
    public String execute(ServiceList serviceList) throws NullPointerException {
        for (Service i : serviceList.getServices()) {
            serviceMap.put(i.getName(), i);
        }
        if (serviceMap.get(searchKey) == null) {
            throw new NullPointerException(ResourceBundle.getBundle("data").getString("ServiceNotFound"));
        } else {
            return serviceMap.get(searchKey).toString();
        }
    }
}
