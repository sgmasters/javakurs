package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.ServiceList;

public interface Operation {
    String execute(ServiceList serviceList) throws NullPointerException;
}
