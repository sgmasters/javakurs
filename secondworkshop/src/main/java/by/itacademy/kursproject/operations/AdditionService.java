package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.domain.Service;
import by.itacademy.kursproject.domain.ServiceList;
import by.itacademy.kursproject.io.SaveToJson;

import java.util.Objects;

public class AdditionService implements Operation {

    private Service service;

    @Override
    public String execute(ServiceList serviceList) throws NullPointerException {
        synchronized (ServiceList.class) {
            serviceList.addService(service);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                new SaveToJson(
                        Objects.requireNonNull(
                                this.getClass().getClassLoader().getResource("data.json")).getFile())
                        .execute(serviceList);

            }
        }).start();
        return "";
    }

    public AdditionService(Service service) {
        this.service = service;
    }
}
