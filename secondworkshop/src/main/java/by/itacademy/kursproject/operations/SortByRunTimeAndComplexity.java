package by.itacademy.kursproject.operations;

import by.itacademy.kursproject.comparators.ByRunTimeAndComplexity;
import by.itacademy.kursproject.menu.menuoperation.SortService;

import java.util.Comparator;

public class SortByRunTimeAndComplexity extends SortService {

    @Override
    protected Comparator getComparator() {
        return new ByRunTimeAndComplexity();
    }

}
