import java.util.Arrays;

public class MethodInputOutput {


    public static int[] input(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i < 10; i++) {
            int[] a = new int[10];
            array[i] = Integer.parseInt(args[i]);
        }
        return array;
    }

    public static String output(int[] ar) {
        return Arrays.toString(ar);
    }

    public static int[] sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
        /*Предполагаем, что первый элемент (в каждом
           подмножестве элементов) является минимальным */
            int min = arr[i];
            int min_i = i;
        /*В оставшейся части подмножества ищем элемент,
           который меньше предположенного минимума*/
            for (int j = i + 1; j < arr.length; j++) {
                //Если находим, запоминаем его индекс
                if (arr[j] < min) {
                    min = arr[j];
                    min_i = j;
                }
            }
        /*Если нашелся элемент, меньший, чем на текущей позиции,
          меняем их местами*/
            if (i != min_i) {
                int tmp = arr[i];
                arr[i] = arr[min_i];
                arr[min_i] = tmp;
            }
        }
        return arr;
    }

    public static void main(String[] args) {
        String[] vvod = {"11", "3", "3", "3", "3", "3", "3", "3", "8", "3"};
        System.out.println(output(sort(input(vvod))));
    }
}
