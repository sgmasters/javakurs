public class StepikmergeArrays {
    public static void main (String[] args ) {
        int[] a1 = {0, 2,7};
        int[] a2 = {1, 3,5};
        int[] a3 = new int[a1.length+a2.length];
        int n1,n2 = 0;
        int [] largerA;
        if (a1.length>a2.length) {
            n1=a2.length;
            n2=a1.length;
            largerA = a1;
        }
        else {
            n1= a1.length;
            n2= a2.length;
            largerA = a2;
        }
        if (n1==n2) {
            if (a1[0] < a2[0]) {
                a3[0] = a1[0];
                a3[1] = a2[0];
            } else {
                a3[0] = a2[0];
                a3[1] = a1[0];
            }
            for (int i = 1; i < n1; i++) {
                if (a1[i] < a3[i]) {
                    for (int j = (n1 + n2 - 2); j >= i; j--) {
                        a3[j + 1] = a3[j];
                    }
                    a3[i] = a1[i];

                } else {
                    for (int j = (n1 + n2 - 2); j >= i + 1; j--) {
                        a3[j + 1] = a3[j];
                    }
                    a3[i + 1] = a1[i];
                }
                if (a2[i] < a3[i]) {
                    for (int j = (n1 + n2 - 2); j >= i; j--) {
                        a3[j + 1] = a3[j];
                    }
                    a3[i] = a2[i];
                } else {

                    if (a3[i + 1] < a2[i]) {
                        int buf = a3[i + 1];
                        a3[i + 1] = a2[i];
                        a2[i] = buf;
                    }
                    for (int j = (n1 + n2 - 2); j >= i + 1; j--) {
                        a3[j + 1] = a3[j];
                    }
                    a3[i + 1] = a2[i];
                }
            }
        } else {
            for (int i = 0; i < n2; i++) {
                if (largerA[n1-1+i] < a3[i]) {
                    for (int j = (n1 + n2 - 2); j >= i; j--) {
                        a3[j+1] = a3[j];
                    }
                    a3[i] = largerA[n1-1+i];
                } else {
                    for (int j = (n1 + n2 - 2); j >= i + 1; j--) {
                        a3[j+1] = a3[j];
                    }
                    a3[i + 1] = largerA[n1-1+i];
                }
            }
        }
        for (int i = 0; i < a3.length-1; i++) {
            System.out.print(a3[i] + ",");
        }
        System.out.print(a3[a3.length-1]);
    }
}
