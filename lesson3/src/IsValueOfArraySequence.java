public class IsValueOfArraySequence {
    public static void main(String[] args) {
        int[] numberSequence = {2,3,4,5,6,7};
        boolean result = true;
        int i=1;
        while (i<numberSequence.length) {
            result &= (numberSequence[i-1]) == (numberSequence[i]-1);
            i++;
        }
        System.out.println(result);
    }
}
