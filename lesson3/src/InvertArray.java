import java.util.Arrays;

public class InvertArray {
    public static void main(String[] args) {
        int[] array = {1,8,6,7,2,36,6};
        int buf = 0;
        System.out.println(Arrays.toString(array));
        for (int i = 0;i<array.length/2;i++) {
            buf =array[i];
            array[i] = array[array.length-1-i];
            array[array.length-1-i]=buf;
        }
        System.out.println(Arrays.toString(array));
    }
}
