import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        int n = 5;
        int[] array = {1,-2,4,-5,1};
        int j = 0;
        int k = 0;

        int sum = 0;
        for (int m=0;m <= n-1;m++) {
            for (int i = m; i <n; i++) {

                while ((i - k - m) >= 0) {
                    sum += array[i - k];
                    k++;
                }
                if (sum < 0) {
                    j++;
                }
                k = 0;
                sum = 0;
            }

        }
        System.out.println(j);
    }
}
