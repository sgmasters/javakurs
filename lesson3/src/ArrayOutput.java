public class ArrayOutput {
    public static void main(String[] args) {
        int[] numbers = {1,3,5,7,9,11,13,15,17,19};
        for (int i = 0; i < numbers.length-1; i++) {
            System.out.print(numbers[i] + ",");
        }
        System.out.print(numbers[numbers.length-1]);
    }
}
