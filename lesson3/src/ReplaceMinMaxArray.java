public class ReplaceMinMaxArray {
    public static void main(String[] args) {
        int[] numbers = {3,3,4,5,5,4,2,7,10,11};
        int minArray = numbers[0];
        int maxArray = numbers[0];
        int minIndex = 0;
        int maxIndex = 0;
        for (int i = 0;i<=numbers.length-1;i++) {
            if (minArray > numbers[i]) {
                minArray = numbers[i];
                minIndex = i;
            }
        }
        for (int i = 0;i<=numbers.length-1;i++) {
            if (maxArray < numbers[i]) {
                maxArray = numbers[i];
                maxIndex = i;
            }
        }
        numbers[minIndex]=0;
        numbers[maxIndex]=99;
        System.out.println("min = " + minArray + " max = " + maxArray);
        System.out.print("[");
        for (int i = 0; i <numbers.length-1; i++) {
            System.out.print(numbers[i] + ",");
        }
        System.out.print(numbers[numbers.length-1]+"]");
    }
}
