public class IsASequence {
    public static void main(String[] args) {
        int numberSequence = 212211;
        boolean result = true;
        while (numberSequence>=10) {
            result &= (numberSequence%100)/10 == (numberSequence%10-1);
            numberSequence/=10;
        }
        System.out.println(result);
    }
}
