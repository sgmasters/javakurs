package by.itacademy.lesson10;

public interface RandomGenerator<T> {
    T execute();
}
