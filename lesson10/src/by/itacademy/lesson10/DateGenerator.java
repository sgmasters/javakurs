package by.itacademy.lesson10;

import java.util.Date;
import java.util.Random;

public class DateGenerator implements RandomGenerator<Date> {

    @Override
    public Date execute() {
        Date date = new Date();
        date.setTime(new Random().nextInt());
        date.setYear(91 + new Random().nextInt(17));
        return date;
    }
}
