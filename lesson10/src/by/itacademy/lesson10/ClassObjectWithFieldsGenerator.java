package by.itacademy.lesson10;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ClassObjectWithFieldsGenerator<T> {
    private Map<Class<?>, RandomGenerator<?>> generators = new HashMap<>();

    public T execute(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        generators.put(int.class, new IntGenerator());
        generators.put(String.class, new StringGenerator());
        generators.put(Date.class, new DateGenerator());
        generators.put(boolean.class, new BooleanGenerator());
        Constructor<T> defaultConstructor = clazz.getConstructor();
        T clazzObject = defaultConstructor.newInstance();
        for (Field f : clazz.getDeclaredFields()) {
            if (f.isAnnotationPresent(Generate.class)) {
                for (Method m : clazz.getMethods()) {
                    if (m.getName().startsWith("set") && m.getName().toLowerCase().endsWith(f.getName().toLowerCase())) {
                        m.invoke(clazzObject, generators.get(f.getType()).execute());
                    }
                }
            }
        }
        return clazzObject;
    }
}
