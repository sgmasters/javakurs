package by.itacademy.lesson10;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) {
        try {
            System.out.println(new ClassObjectWithFieldsGenerator<Student>().execute(Student.class));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
            System.out.println("Чето не так");
        }
    }
}
