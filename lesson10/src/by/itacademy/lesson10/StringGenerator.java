package by.itacademy.lesson10;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class StringGenerator implements RandomGenerator<String> {

    private List<String> names = Arrays.asList(
            "Noah",
            "Liam",
            "William",
            "Mason",
            "James",
            "Benjamin",
            "Jacob",
            "Michael",
            "Elijah",
            "Ethan",
            "Emma",
            "Olivia",
            "Ava",
            "Sophia",
            "Isabella",
            "Mia",
            "Charlotte",
            "Abigail",
            "Harper"
    );

    @Override
    public String execute() {
        return names.get(new Random().nextInt(names.size() - 1));
    }
}
