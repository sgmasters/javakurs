package by.itacademy.lesson10;

import java.util.Date;

public class Student {
    @Generate
    private String name;
    @Generate
    private int age;
    @Generate
    private boolean isDismissed;
    @Generate
    private Date dateOfEntry;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setIsDismissed(boolean dismissed) {
        isDismissed = dismissed;
    }

    public void setDateOfEntry(Date dateOfEntry) {
        this.dateOfEntry = dateOfEntry;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", isDismissed=" + isDismissed +
                ", dateOfEntry=" + dateOfEntry +
                '}';
    }
}
