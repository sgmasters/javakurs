package by.itacademy.javakurs.lesson13.serializers;

import by.itacademy.javakurs.lesson13.Patient;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PatientDeserializer implements JsonDeserializer<Patient> {
    @Override
    public Patient deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Patient patient = new Patient();
        patient.setName(jsonObject.get("name").getAsString());
        patient.setSoName(jsonObject.get("soName").getAsString());
        patient.setDateOfBirth(LocalDate.parse(jsonObject.get("dateOfBirth").getAsString(), DateTimeFormatter.ofPattern("dd-MM-yyyy")));
        patient.setIll(jsonObject.get("isIll").getAsString().startsWith("не"));
        return patient;
    }
}
