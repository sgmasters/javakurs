package by.itacademy.javakurs.lesson13;

import by.itacademy.javakurs.lesson13.actions.Actions;

public class Exit implements Actions {
    @Override
    public void execute(Registry registry) {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "Exit";
    }
}
