package by.itacademy.javakurs.lesson13.actions;

import by.itacademy.javakurs.lesson13.Registry;

public interface Actions {
    void execute(Registry registry);

    String getName();
}
