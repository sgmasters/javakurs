package by.itacademy.javakurs.lesson13;

import by.itacademy.javakurs.lesson13.serializers.RegistryJacksonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(using = RegistryJacksonDeserializer.class)

public class Registry {
    private List<Patient> patients = new ArrayList<>();

    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Registry{patients:");
        s.append(getPatients()).append('}');
        return s.toString();
    }
}
