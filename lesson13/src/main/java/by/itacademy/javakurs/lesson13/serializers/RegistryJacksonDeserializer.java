package by.itacademy.javakurs.lesson13.serializers;

import by.itacademy.javakurs.lesson13.Patient;
import by.itacademy.javakurs.lesson13.Registry;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

public class RegistryJacksonDeserializer extends JsonDeserializer<Registry> {
    @Override
    public Registry deserialize(JsonParser jp, DeserializationContext context) throws IOException, JsonProcessingException {
        Registry registry = new Registry();
        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode patients = node.withArray("patients");
        for (Iterator<JsonNode> it = patients.elements(); it.hasNext(); ) {
            JsonNode p = it.next();
            String name = p.get("name").asText();
            String soName = p.get("soName").asText();
            LocalDate dateOfBirth = LocalDate.parse(p.get("dateOfBirth").asText(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
            boolean isIll = p.get("isIll").asText().startsWith("не");
            registry.addPatient(new Patient(name, soName, dateOfBirth, isIll));
        }
        return registry;
    }
}
