package by.itacademy.javakurs.lesson13.serializers;

import by.itacademy.javakurs.lesson13.Patient;
import by.itacademy.javakurs.lesson13.Registry;
import com.google.gson.*;

import java.lang.reflect.Type;

public class RegistryDeserializer implements JsonDeserializer<Registry> {
    @Override
    public Registry deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        Registry registry = new Registry();
        JsonObject jsonObject = json.getAsJsonObject();
        JsonArray patients = jsonObject.getAsJsonArray("patients");
        for (JsonElement patient : patients) {
            Patient p = context.deserialize(patient, Patient.class);
            registry.addPatient(p);
        }
        return registry;
    }
}
