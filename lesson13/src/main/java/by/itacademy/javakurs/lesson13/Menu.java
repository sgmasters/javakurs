package by.itacademy.javakurs.lesson13;

import by.itacademy.javakurs.lesson13.actions.Actions;
import by.itacademy.javakurs.lesson13.actions.AddByGson;
import by.itacademy.javakurs.lesson13.actions.AddByJackson;
import by.itacademy.javakurs.lesson13.actions.Print;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private static final Scanner SCANNER = new Scanner(System.in);
    private List<Actions> menus = new ArrayList<>(Arrays.asList(
            new AddByGson(),
            new AddByJackson(),
            new SaveToFile(),
            new Print(),
            new Exit()
    ));
    public static Registry registry = new Registry();

    public void execute() {
        StringBuilder s = new StringBuilder("Выберите действия:\n");
        for (int i = 0; i < menus.size(); i++) {
            s.append(i + 1).append(" ").append(menus.get(i).getName()).append("\n");
        }
        System.out.println(s.toString());
        menus.get(Integer.parseInt(SCANNER.next()) - 1).execute(registry);
        this.execute();
    }
}
