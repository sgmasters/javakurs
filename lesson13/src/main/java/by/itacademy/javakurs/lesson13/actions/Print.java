package by.itacademy.javakurs.lesson13.actions;

import by.itacademy.javakurs.lesson13.Registry;

public class Print implements Actions {

    @Override
    public void execute(Registry registry) {
        System.out.println(registry);
    }

    @Override
    public String getName() {
        return "Print";
    }
}
