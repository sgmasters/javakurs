package by.itacademy.javakurs.lesson13.actions;

import by.itacademy.javakurs.lesson13.Menu;
import by.itacademy.javakurs.lesson13.Patient;
import by.itacademy.javakurs.lesson13.Registry;
import by.itacademy.javakurs.lesson13.serializers.PatientDeserializer;
import by.itacademy.javakurs.lesson13.serializers.RegistryDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddByGson implements Actions {

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @Override
    public void execute(Registry registry) {
        try (FileInputStream fis = new FileInputStream(
                Objects.requireNonNull(this.getClass().getClassLoader().getResource("patients.json")).getFile());
             InputStreamReader isr = new InputStreamReader(fis)) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Registry.class, new RegistryDeserializer())
                    .registerTypeAdapter(Patient.class, new PatientDeserializer())
                    .create();
            Menu.registry = gson.fromJson(isr, Registry.class);
        } catch (JsonParseException | IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Add by GSON";
    }
}
