package by.itacademy.javakurs.lesson13;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class Patient {
    private String name;
    private String soName;
    private LocalDate dateOfBirth;
    private boolean isIll;

    public Patient() {
    }

    public Patient(String name, String soName, LocalDate dateOfBirth, boolean isIll) {
        this.name = name;
        this.soName = soName;
        this.dateOfBirth = dateOfBirth;
        this.isIll = isIll;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoName(String soName) {
        this.soName = soName;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public void setIll(boolean ill) {
        isIll = ill;
    }

    public boolean isIll() {
        return isIll;
    }

    public String getName() {
        return name;
    }

    public String getSoName() {
        return soName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("name=");
        s.append(name).append(";").append("soname:").append(soName).append(";")
                .append("date of birth:").append(dateOfBirth.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))
                .append(";").append("isIll:").append(isIll);
        return s.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return isIll == patient.isIll &&
                Objects.equals(name, patient.name) &&
                Objects.equals(soName, patient.soName) &&
                Objects.equals(dateOfBirth, patient.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, soName, dateOfBirth, isIll);
    }
}
