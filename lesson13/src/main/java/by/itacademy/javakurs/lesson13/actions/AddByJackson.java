package by.itacademy.javakurs.lesson13.actions;

import by.itacademy.javakurs.lesson13.Menu;
import by.itacademy.javakurs.lesson13.Registry;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddByJackson implements Actions {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @Override
    public void execute(Registry registry) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Menu.registry = objectMapper.readValue(Objects.requireNonNull(
                    this.getClass().getClassLoader().getResource("patients.json")), Registry.class);
        } catch (IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Add by Jackson";
    }
}
