package by.itacademy.javakurs.lesson11;

import java.util.HashMap;
import java.util.Map;

public class Registry {
    private Map<Patient, Boolean> patients = new HashMap<>();

    public void addPatient(Patient patient, Boolean isIll) {
        patients.put(patient, isIll);
    }

    public Map<Patient, Boolean> getPatients() {
        return patients;
    }

    @Override
    public String toString() {
        return "Registry{" +
                "patients=" + patients +
                '}';
    }
}
