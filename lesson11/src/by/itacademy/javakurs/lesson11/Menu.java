package by.itacademy.javakurs.lesson11;

import by.itacademy.javakurs.lesson11.item.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Menu {
    private static final Scanner SCANNER = new Scanner(System.in);
    private List<Item> items = new ArrayList<>(Arrays.asList(
            new AddFromConsole(),
            new AddFromTextFile(),
            new AddFromBinaryFile(),
            new SaveToFile(),
            new Exit()
    ));
    private Registry registry = new Registry();

    public void execute() {
        StringBuilder s = new StringBuilder("Выберите действия:\n");
        for (int i = 0; i < items.size(); i++) {
            s.append(i + 1).append(" ").append(items.get(i).getName()).append("\n");
        }
        System.out.println(s.toString());
        items.get(Integer.parseInt(SCANNER.next()) - 1).execute(registry);
        this.execute();
    }
}
