package by.itacademy.javakurs.lesson11.item;

import by.itacademy.javakurs.lesson11.Patient;
import by.itacademy.javakurs.lesson11.Registry;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddFromBinaryFile implements Item {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try (FileInputStream fis = new FileInputStream("patients.dat");
             DataInputStream dis = new DataInputStream(fis)) {
            while (fis.available() > 0) {
                registry.addPatient(new Patient(dis.readUTF(), LocalDate.ofEpochDay(dis.readLong()), dis.readUTF()), dis.readBoolean());
            }
            System.out.println(registry);
        } catch (IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "Add patients from binary file";
    }
}
