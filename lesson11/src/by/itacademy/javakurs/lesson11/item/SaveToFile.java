package by.itacademy.javakurs.lesson11.item;

import by.itacademy.javakurs.lesson11.Patient;
import by.itacademy.javakurs.lesson11.Registry;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveToFile implements Item {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("patients.dat"))) {
            for (Patient p : registry.getPatients().keySet()) {
                dataOutputStream.writeUTF(p.getName());
                dataOutputStream.writeLong(p.getDateOfBirth().toEpochDay());
                dataOutputStream.writeUTF(p.getSurname());
                dataOutputStream.writeBoolean(registry.getPatients().get(p));
            }
        } catch (NullPointerException | IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    @Override
    public String getName() {
        return "Save to file";
    }
}
