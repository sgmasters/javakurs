package by.itacademy.javakurs.lesson11.item;

import by.itacademy.javakurs.lesson11.Registry;

public class Exit implements Item {
    @Override
    public void execute(Registry registry) {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "Exit";
    }
}
