package by.itacademy.javakurs.lesson11.item;

import by.itacademy.javakurs.lesson11.Registry;

public interface Item {
    void execute(Registry registry);

    String getName();
}
