package by.itacademy.javakurs.lesson11.item;

import by.itacademy.javakurs.lesson11.Patient;
import by.itacademy.javakurs.lesson11.Registry;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddFromTextFile implements Item {
    private static final Logger LOGGER = Logger.getLogger(SaveToFile.class.getName());

    @Override
    public void execute(Registry registry) {
        try (FileReader fr = new FileReader("patients.txt")) {
            StringBuilder sb = new StringBuilder();
            while (fr.ready()) {
                char[] piece = new char[30];
                fr.read(piece);
                sb.append(piece);
            }
            writeData(registry, sb);
        } catch (IOException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    private void writeData(Registry registry, StringBuilder sb) {
        String str = sb.toString();
        String[] lines = str.replaceAll("\r", "").split("\n");
        for (int i = 0; i < lines.length; i++) {
            String[] s = lines[i].split(";");
            registry.addPatient(new Patient(
                            s[0],
                            LocalDate.parse(s[2], DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                            s[1]),
                    Boolean.valueOf(s[3]));
        }
    }

    @Override
    public String getName() {
        return "Add patients from text file";
    }
}
