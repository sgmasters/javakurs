package by.itacademy.lesson11;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
/*        File file = new File("D:\\students\\Rusak\\javakurs\\lesson11\\src\\by\\itacademy\\lesson11\\notes.txt");
        System.out.println(file.getParent());
        System.out.println((int) file.length());
        Date date = new Date(file.lastModified());
        System.out.println(date);
        */
        Player playerOne = new Player(true, 30);
        Player playerTwo = new Player(false, 30000);
        List<Player> players = new ArrayList<>();
        players.add(playerOne);
        players.add(playerTwo);
        System.out.println(players);
        try (FileInputStream fis = new FileInputStream("D:\\students\\Rusak\\javakurs\\lesson11\\src\\by\\itacademy\\lesson11\\notes2.txt")) {
/*
            while (fis.available()>0) {
                byte[] array = new byte[3];
                fis.read(array);
                System.out.print(new String(array));
            }
            */
            FileOutputStream fos = new FileOutputStream("D:\\students\\Rusak\\javakurs\\lesson11\\src\\by\\itacademy\\lesson11\\notes2.txt");
            /*String text = "blablablacar";
            byte[] textBytes = text.getBytes();
            for (int i=0;i<textBytes.length;i+=4) {
                byte[] bytes  = Arrays.copyOfRange(textBytes,i,i+4);
                fos.write(bytes);
            }
            */
            DataOutputStream dos = new DataOutputStream(fos);
            for (Player p : players) {
                dos.writeBoolean(p.isNewbie());
                dos.writeLong(p.getHoursOfPlay());
            }
            DataInputStream dis = new DataInputStream(fis);
            List<Player> players2 = new ArrayList<>();
            while (fis.available() > 0) {
                Player p = new Player(dis.readBoolean(), dis.readLong());
                players2.add(p);
            }
            System.out.println(players2);
        } catch (FileNotFoundException e) {
        }


    }
}
