package by.itacademy.lesson11;


public class Player {
    private boolean newbie;
    private long hoursOfPlay;

    public boolean isNewbie() {
        return newbie;
    }

    public long getHoursOfPlay() {
        return hoursOfPlay;
    }

    @Override
    public String toString() {
        return "Player{" +
                "newbie=" + newbie +
                ", hoursOfPlay=" + hoursOfPlay +
                '}';
    }

    public Player(boolean newbie, long hoursOfPlay) {
        this.newbie = newbie;
        this.hoursOfPlay = hoursOfPlay;

    }
}
