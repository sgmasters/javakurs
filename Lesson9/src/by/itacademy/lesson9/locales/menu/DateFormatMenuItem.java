package by.itacademy.lesson9.locales.menu;

import by.itacademy.lesson9.locales.DateFormats;
import sun.util.locale.provider.LocaleProviderAdapter;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class DateFormatMenuItem implements MenuItem {
    private final BySymbols bySymbols = new BySymbols();
    private RootMenuItem rootMenuItem;
    private Scanner scanner = new Scanner(System.in);
    private DateFormats dateFormats = new DateFormats();

    public DateFormatMenuItem(RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
    }

    @Override
    public void execute() {
        StringBuilder show = new StringBuilder("Выберите тип даты: \n");
        for (int i = 0; i < dateFormats.size(); i++) {
            show.append(i + 1).append(". ").append(dateFormats.get(i)).append("\n");
        }
        System.out.print(show);
        int choice = scanner.nextInt();

        Locale locale = rootMenuItem.getLocale();
        Date currentDate = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(LocaleProviderAdapter.getResourceBundleBased().getLocaleResources(locale)
                .getDateTimePattern(choice - 1, choice - 1, Calendar.getInstance()), locale);
        if (locale.getDisplayName().equals("by")) {
            changeFormatSymbols(simpleDateFormat, bySymbols);
        }
        System.out.println(simpleDateFormat.format(currentDate));
    }

    private void changeFormatSymbols(SimpleDateFormat simpleDateFormat, FormatSymbols symbols) {
        DateFormatSymbols defaultSymbols = simpleDateFormat.getDateFormatSymbols();
        defaultSymbols.setMonths(symbols.getFormatByDateSymbols());
        defaultSymbols.setShortMonths(symbols.getFormatByShortDateSymbols());
        defaultSymbols.setAmPmStrings(symbols.getFormatByAmPmSymbols());
        defaultSymbols.setWeekdays(symbols.getFormatDaysSymbols());
        defaultSymbols.setZoneStrings(symbols.getByZoneStrings());
        simpleDateFormat.setDateFormatSymbols(defaultSymbols);
    }
}