package by.itacademy.lesson9.locales.menu;

public class BySymbols implements FormatSymbols {
    private final String[] formatByDateSymbols = new String[]{
            "студзень",
            "люты",
            "сакавік",
            "красавік",
            "май",
            "чэрвень",
            "ліпень",
            "жнівень",
            "верасень",
            "кастрычнік",
            "лістапад",
            "снежань"
    };
    private final String[] formatByShortDateSymbols = new String[]{
            "студ",
            "лют",
            "сак",
            "крас",
            "май",
            "чэрв",
            "ліп",
            "жнів",
            "вер",
            "кастр",
            "ліс",
            "снеж"
    };
    private final String[] formatByAmPmSymbols = new String[]{
            "",
            ""
    };
    private final String[] formatDaysSymbols = new String[]{
            "панядзелак",
            "аўторак",
            "серада",
            "чацьвер",
            "пятніца",
            "сыбота",
            "нядзеля"
    };
    private final String[][] byZoneStrings = new String[5][5];


    @Override
    public String[] getFormatByDateSymbols() {
        return formatByDateSymbols;
    }

    @Override
    public String[] getFormatByShortDateSymbols() {
        return formatByShortDateSymbols;
    }

    @Override
    public String[] getFormatByAmPmSymbols() {
        return formatByAmPmSymbols;
    }

    @Override
    public String[] getFormatDaysSymbols() {
        return formatDaysSymbols;
    }

    @Override
    public String[][] getByZoneStrings() {
        return byZoneStrings;
    }
}