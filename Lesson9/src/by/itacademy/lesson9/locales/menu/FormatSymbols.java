package by.itacademy.lesson9.locales.menu;

public interface FormatSymbols {
    String[] getFormatByDateSymbols();

    String[] getFormatByShortDateSymbols();

    String[] getFormatByAmPmSymbols();

    String[] getFormatDaysSymbols();

    String[][] getByZoneStrings();
}
