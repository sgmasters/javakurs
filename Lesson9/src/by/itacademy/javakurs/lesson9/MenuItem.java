package by.itacademy.javakurs.lesson9;

public interface MenuItem {
    void execute();
}
