package by.itacademy.javakurs.lesson9;


import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class LocalChoiceMenuItem implements MenuItem, RootMenuItem {
    private Scanner sc = new Scanner(System.in);
    private MenuItem next = new DateFormatMenuItem(this);
    private Locale localechoice;

    public void execute() {
        Locales allLocale = new Locales();
        List<Locale> locales = allLocale.get();
        StringBuilder s = new StringBuilder("Выберите локаль\n");
        for (int i =0;i<locales.size();i++) {
            s.append(i+1).append(". ").append(locales.get(i)).append("\n");
        }
        System.out.println(s.toString());
        localechoice = locales.get(sc.nextInt()-1);
        ResourceBundle bundle = ResourceBundle.getBundle("resource", localechoice);
        System.out.println(bundle.getString("hello"));
        next.execute();
    }
    public Locale getLocale() {
        return localechoice;
    }
}
