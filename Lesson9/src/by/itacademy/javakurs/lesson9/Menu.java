package by.itacademy.javakurs.lesson9;

public class Menu {

    private MenuItem next = new LocalChoiceMenuItem();

    public void execute() {
        next.execute();
    }

}
