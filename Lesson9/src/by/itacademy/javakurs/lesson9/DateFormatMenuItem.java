package by.itacademy.javakurs.lesson9;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateFormatMenuItem implements MenuItem {

    private RootMenuItem rootMenuItem;
    private LocalDateTime localDateTime = LocalDateTime.now();
    public DateFormatMenuItem(RootMenuItem rootMenuItem) {
        this.rootMenuItem = rootMenuItem;
    }

    @Override
    public void execute() {
        LocalDate localDate = LocalDate.now();
        String s = localDateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM).withLocale(rootMenuItem.getLocale()));
        String date = localDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).withLocale(rootMenuItem.getLocale()));
        System.out.println(s);
        System.out.println(date);
    }
}
