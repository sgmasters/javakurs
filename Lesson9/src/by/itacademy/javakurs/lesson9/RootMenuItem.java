package by.itacademy.javakurs.lesson9;


import java.util.Locale;

public interface RootMenuItem {
    Locale getLocale();
}
