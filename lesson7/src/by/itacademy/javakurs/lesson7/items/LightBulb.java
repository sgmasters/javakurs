package by.itacademy.javakurs.lesson7.items;

public class LightBulb {
    private int power;

    public LightBulb(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }
}
