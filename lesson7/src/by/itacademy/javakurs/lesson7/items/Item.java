package by.itacademy.javakurs.lesson7.items;

public interface Item {
    double getSquare();
}
