package by.itacademy.javakurs.lesson7.items;

public class Chair implements Item {
    private String name;
    private double square;

    public Chair(String name, double square) {
        this.name = name;
        this.square = square;
    }

    @Override
    public double getSquare() {
        return square;
    }

    @Override
    public String toString() {
        return name + " (площадь " + square + "м^2)";
    }
}
