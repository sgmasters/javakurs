package by.itacademy.javakurs.lesson7.exceptions;

public class SpaceLimitException extends Throwable {
    public SpaceLimitException(String message) {
        super(message);
    }
}
