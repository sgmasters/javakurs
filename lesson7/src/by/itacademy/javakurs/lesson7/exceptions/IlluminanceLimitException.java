package by.itacademy.javakurs.lesson7.exceptions;

public class IlluminanceLimitException extends Throwable {
    public IlluminanceLimitException(String message) {
        super(message);
    }
}
