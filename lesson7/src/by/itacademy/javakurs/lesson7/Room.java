package by.itacademy.javakurs.lesson7;

import by.itacademy.javakurs.lesson7.exceptions.IlluminanceLimitException;
import by.itacademy.javakurs.lesson7.exceptions.SpaceLimitException;
import by.itacademy.javakurs.lesson7.items.Item;
import by.itacademy.javakurs.lesson7.items.LightBulb;
import by.itacademy.lesson7.ArrayList;
import by.itacademy.lesson7.List;

public class Room {
    private String name;
    private double square;
    private int numberOfWindows;
    private List<LightBulb> lightBulbs = new ArrayList<>();
    private List<Item> items = new ArrayList<>();

    public Room(String name, double square, int numberOfWindows) {
        this.name = name;
        this.square = square;
        this.numberOfWindows = numberOfWindows;
    }

    public void addLightBulb(LightBulb lightBulb) throws IlluminanceLimitException {
        lightBulbs.add(lightBulb);
        if (getLuminosity() > 4000 || getLuminosity() < 300) {
            throw new IlluminanceLimitException("Превышен лимит освещенности");
        }
    }

    public void addItem(Item item) throws SpaceLimitException {
        items.add(item);
        if (getFreeSpace() / square < 0.3) {
            throw new SpaceLimitException("Недостаточно свободного места");
        }
    }

    private int getLuminosity() {
        int luminosity = numberOfWindows * 700;
        for (LightBulb i : lightBulbs) {
            luminosity += i.getPower();
        }
        return luminosity;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder(name);
        s.append("\n ").append(" Освещенность = ").append(getLuminosity());
        s.append("лк (").append("окна ").append(numberOfWindows).append(" по 700лк, лампочки");
        for (LightBulb i : lightBulbs) {
            s.append(" ").append(i.getPower()).append("лк");
        }
        s.append(")");
        s.append("\n  Площадь = ").append(square).append("м^2 (занято ");
        s.append(square - getFreeSpace()).append("м^2, свободно ").append(getFreeSpace()).append("м^2 или ");
        s.append(100 * getFreeSpace() / square).append("% площади)").append("\n");
        s.append("  Мебель:\n   ");
        for (Item i : items) {
            s.append(i.toString()).append("\n   ");
        }
        s.append("\n");
        return s.toString();
    }

    private double getFreeSpace() {
        double free = square;
        for (Item i : items) {
            free -= i.getSquare();
        }
        return free;
    }
}
