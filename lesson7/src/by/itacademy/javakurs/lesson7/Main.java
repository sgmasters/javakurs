package by.itacademy.javakurs.lesson7;

import by.itacademy.javakurs.lesson7.exceptions.IlluminanceLimitException;
import by.itacademy.javakurs.lesson7.exceptions.SpaceLimitException;
import by.itacademy.javakurs.lesson7.items.Chair;
import by.itacademy.javakurs.lesson7.items.LightBulb;
import by.itacademy.javakurs.lesson7.items.Table;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        final Logger logger = Logger.getLogger(Main.class.getName());
        Building building = new Building("Здание 1");
        building.addRoom("Кухня", 4, 1);
        building.addRoom("Спальня", 10, 3);
        try {
            building.addItem(0, new Chair("Стул", 5));
            building.addItem(0, new Table("Стол обеденный", 2));
            building.addItem(1, new Chair("Кресло качалка", 1));
            building.addLightBulb(0, new LightBulb(150));
            building.addLightBulb(1, new LightBulb(100));
            building.addLightBulb(1, new LightBulb(400));
            building.info();
        } catch (IlluminanceLimitException | SpaceLimitException e) {
            logger.log(Level.INFO, e.getMessage());
        }
    }
}
