package by.itacademy.javakurs.lesson7;

import by.itacademy.javakurs.lesson7.exceptions.IlluminanceLimitException;
import by.itacademy.javakurs.lesson7.exceptions.SpaceLimitException;
import by.itacademy.javakurs.lesson7.items.Item;
import by.itacademy.javakurs.lesson7.items.LightBulb;
import by.itacademy.lesson7.ArrayList;
import by.itacademy.lesson7.List;

public class Building {
    private String name;
    private List<Room> rooms = new ArrayList<>();

    public Building(String name) {
        this.name = name;
    }

    public void addRoom(String name, double square, int numberOfWindows) {
        rooms.add(new Room(name, square, numberOfWindows));
    }

    public void addItem(int index, Item item) throws SpaceLimitException {
        rooms.get(index).addItem(item);
    }

    public void addLightBulb(int index, LightBulb lightBulb) throws IlluminanceLimitException {
        rooms.get(index).addLightBulb(lightBulb);
    }

    public void info() {
        StringBuilder s = new StringBuilder(name);
        s.append("\n ");
        for (Room room : rooms) {
            s.append(room.toString());
        }
        System.out.println(s.toString());
    }
}
