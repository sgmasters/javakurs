package by.itacademy.lesson7;

/**
 * Created by user on 25.09.2018.
 */
public interface List<T> extends Iterable<T> {
    T get(int index);
    int size();
    void add(T t);
    void remove(int index);
}
