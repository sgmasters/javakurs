package by.itacademy.lesson7;

import java.util.Iterator;


public class ArrayList<E> implements List<E> {
    private static final int DEFAULT_CAPACITY = 10;
    private E[] arrayList;
    private int index = -1;

    public ArrayList() {
        arrayList = (E[]) new Object[DEFAULT_CAPACITY];
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int index = 0;

            @Override
            public boolean hasNext() {
                return index <= size()-1;
            }

            @Override
            public E next() {
                return arrayList[index++];
            }
        };
    }

    @Override
    public E get(int index) {
        return arrayList[index];
    }


    @Override
    public int size() {
        return index +1;
    }

    @Override
    public void add(E e) {
        if (index >= arrayList.length) {
            E[] newArrayList = (E[]) new Object[arrayList.length+DEFAULT_CAPACITY];
            System.arraycopy(arrayList,0,newArrayList,0,arrayList.length);
            arrayList = newArrayList;
        }
        index++;
        arrayList[index]= e;
    }

    @Override
    public void remove(int index) {
        System.arraycopy(arrayList,index+1,arrayList,index,arrayList.length-1-index);
        this.index--;
    }



}
