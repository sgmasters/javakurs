package by.itacademy.lesson7;

import java.util.Iterator;


public class LinkedList<E>  implements List<E> {
    private Node<E> first;
    private Node<E> last;
    private int size=0;

    public LinkedList() {
    }

    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    @Override
    public E get(int index) {
        return nodeByIndex(index).item;
    }

    private Node<E> nodeByIndex(int index) {
        Node<E> x = first;
        for (int i = 0; i < index; i++) {
            x = x.next;
        }
        return x;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
    }

    @Override
    public void remove(int index) {
        Node<E> x = nodeByIndex(index);
        if (x.prev == null) {
            first = x.next;
        } else {
            x.prev.next = x.next;
            x.prev = null;
        }
        if (x.next == null) {
            last = x.prev;
        } else {
            x.next.prev = x.prev;
            x.next = null;
        }
        x.item = null;
        size--;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private int index = 0;
            @Override
            public boolean hasNext() {
                return index<=size - 1;
            }

            @Override
            public E next() {
                return get(index++);
            }
        };
    }
}
