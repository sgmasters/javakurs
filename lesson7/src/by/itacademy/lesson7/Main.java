package by.itacademy.lesson7;


public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(2);
        numbers.add(1);
        for (Integer number: numbers) {
            System.out.println(number);
        }
        System.out.println("Размер массива: " + numbers.size());
        System.out.println("Элемент по индексу 1: " + numbers.get(2));
        numbers.remove(0);
        System.out.println("List без элемента с индексом 0");
        for (Integer number: numbers) {
            System.out.println(number);
        }
        System.out.println("_____________________");
        LinkedList<Integer> linkedNumbers = new LinkedList<>();
        linkedNumbers.add(5);
        linkedNumbers.add(2);
        linkedNumbers.add(1);
        for (Integer number: linkedNumbers) {
            System.out.println(number);
        }
        System.out.println("Размер массива: " + linkedNumbers.size());
        System.out.println("Элемент по индексу 2: " + linkedNumbers.get(2));
        linkedNumbers.remove(0);
        System.out.println("List без элемента с индексом 0");
        for (Integer number: linkedNumbers) {
            System.out.println(number);
        }
    }

}
