package by.itacademy.lesson6.menu;

import java.util.Scanner;

public class MenuDisplay implements RootMenuItem {


    private static final Scanner SCANNER = new Scanner(System.in);

    private MenuItem[] subMenus = {
            new MenuAddition(this),
            new MenuDivision(this),
            new MenuExhibitor(this),
            new MenuMultiply(this),
            new MenuSquareRoot(this),
            new MenuSubstruction(this),
            new MenuExit()
    };

    @Override
    public void execute() {
        for (int i = 0; i < subMenus.length; i++) {
            System.out.println((i+1) + " " + subMenus[i].name());
        }
        subMenus[SCANNER.nextInt()-1].execute();

    }
}

