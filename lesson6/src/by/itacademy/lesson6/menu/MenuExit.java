package by.itacademy.lesson6.menu;


public class MenuExit implements MenuItem {

    @Override
    public void execute() {
        return;
    }

    public String name() {
        return "Exit";
    }
}
