package by.itacademy.lesson6.menu;

public interface RootMenuItem {
    void execute();
}
