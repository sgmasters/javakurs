package by.itacademy.lesson6.menu;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;
import by.itacademy.lesson6.operations.Operation;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class MenuCommonOperation implements MenuItem{
    private static final Logger LOGGER =
            Logger.getLogger(MenuCommonOperation.class.getName());

    private Operation operation;
    private RootMenuItem rootMenuItem;
    private static final Scanner SCANNER = new Scanner(System.in);

    public MenuCommonOperation(Operation operations,RootMenuItem rootMenuItem) {
        this.operation = operations;
        this.rootMenuItem = rootMenuItem;
    }



    @Override
    public  void execute(){
        double[] arguments=new double[operandsCount()]; //кол-во аргументов в функции, сложение-2, корень-1
        for (int i = 0; i < operandsCount(); i++) {    //наполнение массива аргументами в зависимости от кол-ва
            System.out.println("Введите опреанд: ");   //в каждой вункции свой массив
            arguments[i] = SCANNER.nextDouble();
        }
        try {
            Operands operands = operandsType(arguments);
            System.out.println(operation.calculate(operands));
            rootMenuItem.execute();
        } catch (OperandsBoundsException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }

    }

    protected abstract int operandsCount();
    protected abstract Operands operandsType(double ...abc);

}



