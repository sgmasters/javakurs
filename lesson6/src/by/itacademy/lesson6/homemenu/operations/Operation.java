package by.itacademy.lesson6.homemenu.operations;

import by.itacademy.lesson6.homemenu.operands.Operands;
import by.itacademy.lesson6.homemenu.operands.OperandsBoundsException;

public interface Operation {
    double calculate(Operands operands) throws OperandsBoundsException;
}
