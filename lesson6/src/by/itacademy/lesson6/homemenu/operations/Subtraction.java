package by.itacademy.lesson6.homemenu.operations;

import by.itacademy.lesson6.homemenu.operands.Operands;
import by.itacademy.lesson6.homemenu.operands.OperandsBoundsException;

public class Subtraction implements Operation {
    @Override
    public double calculate(Operands operands) throws OperandsBoundsException {
        return operands.get(0) - operands.get(1);
    }
}
