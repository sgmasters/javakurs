package by.itacademy.lesson6.homemenu.operations;

import by.itacademy.lesson6.homemenu.operands.Operands;
import by.itacademy.lesson6.homemenu.operands.OperandsBoundsException;

public class SquareRoot implements Operation {
    @Override
    public double calculate(Operands operands) throws OperandsBoundsException {
        return Math.sqrt(operands.get(0));
    }
}
