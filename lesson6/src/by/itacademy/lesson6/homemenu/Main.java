package by.itacademy.lesson6.homemenu;

import by.itacademy.lesson6.homemenu.menu.Menu;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;

public class Main {

    public static void main(String[] args) throws IOException {
        SimpleCalculator calculator = new SimpleCalculator();
        try {
            Menu menu = new Menu();
            if (menu.getChoice() != 7) {
                calculator.execute(menu.getChoice() - 1, calculator.getOperands(menu.getChoice()));
            }
        } catch (IllegalIndexOfMenu e) {
            FileHandler handler = new FileHandler("log.%u.%g.txt");
            SimpleCalculator.LOGGER.log(Level.INFO, e.getMessage(), e);
        }

    }
}