package by.itacademy.lesson6.homemenu.operands;

public class OperandsOne implements Operands {
    private double a;

    public OperandsOne(double a) {
        this.a = a;
    }

    public double get(int index) {
        return a;
    }
}
