package by.itacademy.lesson6.homemenu.operands;

public interface Operands {
    double get(int index) throws OperandsBoundsException;
}
