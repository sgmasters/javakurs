package by.itacademy.lesson6.homemenu.operands;

public class OperandsBoundsException extends Exception {
    public OperandsBoundsException(String message) {
        super(message);
    }
}
