package by.itacademy.lesson6.homemenu.menu;


import by.itacademy.lesson6.homemenu.IllegalIndexOfMenu;
import by.itacademy.lesson6.homemenu.SimpleCalculator;

public class Menu {

    private String[] entries = {
            "Addition",
            "Substraction",
            "Myltiply",
            "Division",
            "Square Root",
            "Exhibitor",
            "Exit"
    };

    private int choice;

    public Menu()  throws IllegalIndexOfMenu {
        StringBuilder s = new StringBuilder("Choose operation:\n-------------------------\n");
        for (int i = 0; i < entries.length; i++) {
            s.append(i + 1).append(" - ").append(entries[i]).append("\n");
        }
        System.out.println(s);
        choice = SimpleCalculator.SCANNER.nextInt();
        if (choice < 7 && choice >= 0) {
            new MenuOperands(entries[choice - 1]);
        } else if (choice != 7) {
            throw new IllegalIndexOfMenu("Incorrect item of menu");
        }
    }

    public int getChoice() {
        return choice;
    }

}
