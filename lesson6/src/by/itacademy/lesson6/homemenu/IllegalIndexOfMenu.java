package by.itacademy.lesson6.homemenu;

public class IllegalIndexOfMenu extends Exception {
    public IllegalIndexOfMenu(String message) {
        super(message);
    }
}
