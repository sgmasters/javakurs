package by.itacademy.lesson6.homemenu;

import by.itacademy.lesson6.homemenu.operands.Operands;
import by.itacademy.lesson6.homemenu.operands.OperandsBoundsException;
import by.itacademy.lesson6.homemenu.operands.OperandsOne;
import by.itacademy.lesson6.homemenu.operands.OperandsTwo;
import by.itacademy.lesson6.homemenu.operations.Addition;
import by.itacademy.lesson6.homemenu.operations.Division;
import by.itacademy.lesson6.homemenu.operations.Exhibitor;
import by.itacademy.lesson6.homemenu.operations.Multiply;
import by.itacademy.lesson6.homemenu.operations.Operation;
import by.itacademy.lesson6.homemenu.operations.SquareRoot;
import by.itacademy.lesson6.homemenu.operations.Subtraction;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimpleCalculator {
    public static final Logger LOGGER = Logger.getLogger(SimpleCalculator.class.getName());
    public static final Scanner SCANNER = new Scanner(System.in);

    private Operation[] operations = {
            new Addition(),
            new Subtraction(),
            new Multiply(),
            new Division(),
            new SquareRoot(),
            new Exhibitor()
    };

    public Operands getOperands(int index) {
        if (index>=0&&index<5) {
            return new OperandsTwo(SCANNER.nextInt(),SCANNER.nextInt());
        } else if (index>=5&&index<7) {
            return new OperandsOne(SCANNER.nextInt());
        } else {
            return new OperandsOne(SCANNER.nextInt());
        }
    }

    public void execute(int index, Operands operands) {
        try {
            System.out.println("result = " + operations[index].calculate(operands));
        } catch (OperandsBoundsException e) {
            LOGGER.log(Level.INFO, e.getMessage(), e);
        }
    }


}
