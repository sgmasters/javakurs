package by.itacademy.lesson6.operations;

import by.itacademy.lesson6.operands.Operands;
import by.itacademy.lesson6.operands.OperandsBoundsException;

public interface Operation {
    double calculate(Operands operands) throws OperandsBoundsException;
}
