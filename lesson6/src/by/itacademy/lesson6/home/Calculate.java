package by.itacademy.lesson6.home;

public interface Calculate {
    void calculate() throws IllegalFormatException, IllegalOperator;
}
