package by.itacademy.lesson6.home;

public class IllegalFormatException extends RuntimeException {
    public IllegalFormatException(String message) {
        super(message);
    }

}
