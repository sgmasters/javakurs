package by.itacademy.lesson6.home;

public interface TwoOperand {
    double operation(double a,double b);
}
