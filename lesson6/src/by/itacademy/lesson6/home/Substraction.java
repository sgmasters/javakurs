package by.itacademy.lesson6.home;

public class Substraction implements TwoOperand {
    @Override
    public double operation(double a, double b) {
        return a-b;
    }
}
