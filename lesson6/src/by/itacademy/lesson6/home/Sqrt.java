package by.itacademy.lesson6.home;

public class Sqrt implements OneOperand {

    @Override
    public double operation(double a) {
        return Math.sqrt(a);
    }
}
