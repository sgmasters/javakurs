package by.itacademy.lesson6.home;

import java.util.Scanner;

public class Calculator implements Calculate{
    private double result;
    private static final Scanner SCANNER = new Scanner(System.in);

    @Override
    public void calculate() throws IllegalFormatException, IllegalOperator {
        double chislo1=1;
        String operation="";
        double chislo2=1;
        if (SCANNER.hasNextDouble()) {
                chislo1 = SCANNER.nextDouble();
                operation = SCANNER.next();
                chislo2 = SCANNER.nextDouble();
            } else if (SCANNER.hasNextDouble()) {
            throw new IllegalFormatException("Unsupported format");
        } else {
            operation = SCANNER.next();
            chislo1 = SCANNER.nextDouble();
        }
        if (!operation.contains("*")&&!operation.contains("/")&&!operation.contains("+")&&!operation.contains("-")&&!operation.contains("sqrt")&&!operation.contains("pow")) {
            throw new IllegalOperator("Incorrect operator");
        }
        if (operation.equals("*")) {
            Multiply m = new Multiply();
            result = m.operation(chislo1,chislo2);
        }
        if (operation.equals("/")) {
            Division div = new Division();
            result = div.operation(chislo1,chislo2);
        }
        if (operation.equals("sqrt")) {
            Sqrt sqrt = new Sqrt();
            result = sqrt.operation(chislo1);
        }
        if (operation.equals("+")) {
            Addition add = new Addition();
            result = add.operation(chislo1,chislo2);
        }
        if (operation.equals("-")) {
            Substraction sub = new Substraction();
            result = sub.operation(chislo1,chislo2);
        }
        if (operation.equals("exp")) {
            Exponent exp = new Exponent();
            result = exp.operation(chislo1);
        }
        System.out.printf("%2f", result);
    }


}
