package by.itacademy.lesson6.home;

public class Exponent implements OneOperand{
    @Override
    public double operation(double a) {
        return Math.exp(a);
    }
}
