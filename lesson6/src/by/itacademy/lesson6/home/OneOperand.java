package by.itacademy.lesson6.home;

public interface OneOperand {
    double operation(double a);
}
