package by.itacademy.lesson6.operands;

public class OperandsBoundsException extends Exception {
    public OperandsBoundsException(String message) {
        super(message);
    }
}
