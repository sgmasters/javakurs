package by.itacademy.lesson6.operands;

public interface Operands {
    double get(int index) throws OperandsBoundsException;
}
