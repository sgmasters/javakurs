import javax.xml.bind.SchemaOutputResolver;

public class CashMachine implements AdjunctionCash, ReceivingCash {
    private int cashValue;
    private Notes bankCash = new Notes(cashValue);

    public CashMachine(int cashValue) {
        if (checkCorrectSum(cashValue)) {
            this.cashValue = cashValue;
        } else {
            System.out.println("Incorrect number of cash");
        }
    }

    private boolean checkCorrectSum(int value) {
        return value % 100 != 10 && value % 100 != 30 && value % 100 != 90;
    }

    public int getCashValue() {
        return cashValue;
    }

    @Override
    public void AdjunctionCash(int cash) {
        bankCash.substractCash(cash);
        cashValue-=cash;
    }

    @Override
    public void ReceivingCash(int cash) {
        bankCash.addingCash(cash);
        cashValue+=cash;
    }
}
