public class Notes {
    private int numberOf20;
    private int numberOf50;
    private int numberOf100;

    public Notes(int cash) {
        this.numberOf20 = sortCash(cash)[0];
        this.numberOf50 = sortCash(cash)[1];
        this.numberOf100 = sortCash(cash)[2];
    }

    public int[] sortCash(int value) {
        int numberOf100 = numberOfValue( value,100);
        value -= numberOf100*100;
        int numberOf50 = numberOfValue( value,50);
        value -= numberOf50*50;
        int numberOf20 = numberOfValue( value,20);
        value -= numberOf20*20;
        int[] result = {numberOf100,numberOf50,numberOf20};
        return result;
    }

    private int numberOfValue(int cash, int denomination) {
        int number =0;
        while (cash >= denomination) {
            cash -=denomination;
            number++;
        }
        return number;
    }

    public void addingCash(int value) {
        numberOf100+=sortCash(value)[0];
        numberOf50+=sortCash(value)[1];
        numberOf20+=sortCash(value)[2];
    }

    public void substractCash(int value) {

        numberOf100-=sortCash(value)[0];
        numberOf50-=sortCash(value)[1];
        numberOf20-=sortCash(value)[2];
    }
}
